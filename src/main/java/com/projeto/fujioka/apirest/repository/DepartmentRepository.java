package com.projeto.fujioka.apirest.repository;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.fujioka.apirest.domain.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

	Optional<Department> findById(Long id);

	
}
