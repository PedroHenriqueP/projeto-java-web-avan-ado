package com.projeto.fujioka.apirest.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.fujioka.apirest.domain.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

	List<Person> findAll();

	Optional<Person> findById(long id);

	Person save(Person person);

	void delete(Person person);

}
