package com.projeto.fujioka.apirest.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.fujioka.apirest.domain.Person;
import com.projeto.fujioka.apirest.repository.PersonRepository;

@RestController
@RequestMapping(value="/api")
public class PersonRest {

	@Autowired
	PersonRepository personRepository;
	
	//listagem de todos os persons
	@GetMapping("/persons")
	public List<Person> listaPersons(){
		return personRepository.findAll();
	}
	
	//listagem de um person em específico
	@GetMapping("/person/{id}")
	public Optional<Person> listaUmPerson(@PathVariable(value="id") long id) {
		return personRepository.findById(id);
	}
	
	//criar/salvar um person 
	//não se add um id na mão, pois ja é criado um automaticamente
	@PostMapping("/person")
	public Person salvarPerson(@RequestBody Person person) {
		return personRepository.save(person);
	}
	
	//deletando um person
	@DeleteMapping("/person")
	public void deletarPerson(@RequestBody Person person) {
		personRepository.delete(person);
	}
	
	//atualizar um person
	@PutMapping("/person")
	public Person atualizarPerson(@RequestBody Person person) {
		return personRepository.save(person);
	}
	
}
