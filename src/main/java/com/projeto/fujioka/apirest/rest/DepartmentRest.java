package com.projeto.fujioka.apirest.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.fujioka.apirest.domain.Department;
import com.projeto.fujioka.apirest.repository.DepartmentRepository;

@RestController
@RequestMapping(value="/api")
public class DepartmentRest {

	@Autowired
	DepartmentRepository departmentRepository;
	
	//listagem de todos os departments
	@GetMapping("/departments")
	public List<Department> listaDepartments(){
		return departmentRepository.findAll();
	}
	
	//listagem de um department em específico
	@GetMapping("/department/{id}")
	public Optional<Department> listaUmDepartment(@PathVariable(value="id") long id) {
		return departmentRepository.findById(id);
	}
	
	//criar/salvar um department 
	//não se add um id na mão, pois ja é criado um automaticamente
	@PostMapping("/department")
	public Department salvarDepartment(@RequestBody Department department) {
		return departmentRepository.save(department);
	}
	
	//deletando um department
	@DeleteMapping("/department")
	public void deletarDepartment(@RequestBody Department department) {
		departmentRepository.delete(department);
	}
	
	//atualizar um department
	@PutMapping("/department")
	public Department atualizarDepartment(@RequestBody Department department) {
		return departmentRepository.save(department);
	}
}
